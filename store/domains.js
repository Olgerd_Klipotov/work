import api from '@/api';

var DomainDescriptions = {
  Country: {
    url: '/countries/getAll',
  },
  Brand: {
    url: '/brands/getAll',
  },
  VehicleType: {
    url: '/vehicleTypes/getAll',
  },
  VehicleModel: {
    url: '/vehicleModels/getAll',
  },
};

export default {
  namespaced: true,
  state() {
    return {};
  },
  mutations: {
    updated(state, payload) {
      Vue.set(state, payload.name, payload.items);
    },
  },
  actions: {
    async updateAll(context) {
      var names = Object.keys(DomainDescriptions);
      await Promise.all(names.map(name => context.dispatch('update', name)));
      // а это не по урлам.
      context.commit('updated', {
        name: 'ArticleCategories',
        items: [
          { id: 'Review', text: 'Review' },
          { id: 'TestDrive', text: 'TestDrive' },
          { id: 'Report', text: 'Report' },
          { id: 'Announce', text: 'Announce' },
          { id: 'Accessory', text: 'Accessory' },
          { id: 'Industry', text: 'Industry' },
        ],
      });
    },
    async update(context, name) {
      try {
        var description = DomainDescriptions[name];
        if (!description) {
          throw new Error(`Домена ${name} не существует`);
        }

        var { data: { data: items } } = await api.get(description.url);
        context.commit('updated', { name, items });
      }
      catch (err) {
        console.error(err);
      }
    },
  },
};
