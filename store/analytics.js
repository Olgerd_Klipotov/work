import api from '@/api';

export const state = () => ({
  counter: null,
});

export const mutations = {
  counterUpdated(state, payload) {
    state.counter = payload.counter;
  },
};

export const actions = {
  async updateCounter(context) {
    let { data: { data: counters } } = await api.get(`/analyticalScripts/getAll`);
    let counter = _.find(counters, { name: 'main' });
    if (!counter) {
      counter = null;
    }

    context.commit('counterUpdated', { counter });
  },
};
