import { get, post, setToken, clearToken } from '@/api';

export function state() {
  return {
    inProgress: false,
    account: null,
  };
}

export const mutations = {
  authenticationStarted(state, payload) {
    state.inProgress = true;
  },
  authenticationSucceeded(state, payload) {
    state.inProgress = false;
    state.account = payload.account;
  },
  authenticationFailed(state, payload) {
    state.inProgress = false;
  },
  authenticationCanceled(state, payload) {
    state.inProgress = false;
  },
};

export const actions = {
  async fakeSignIn(context) {
    context.commit('authenticationStarted');
    var account = {
      id: '1',
      // @TODO: заполнять по мере необходимости
    };
    context.commit('authenticationSucceeded', { account: account });
  },
  async fakeSignOut(context) {
    context.commit('authenticationStarted');
    context.commit('authenticationSucceeded', { account: null });
  },
  async update(context) {
    try {
      context.commit('authenticationStarted');
      var response = await get('/accounts/current');
      var account = response.data.data;
      context.commit('authenticationSucceeded', { account: account });
    }
    catch (err) {
      context.commit('authenticationFailed');
      // @NOTE: неуспешный update это не ошибка, поэтому не бросаем
    }
  },
  async register(context, userInfo) {
    try {
      context.commit('authenticationStarted');
      var registerResponse = await post('/accounts/register', userInfo);
      var account = registerResponse.data.data;

      // @NOTE: предполагаем, что можно входить сразу после регистрации, без подтверждения аккаунта
      var credentials = {
        login: userInfo.nickName,
        password: userInfo.password,
      };
      var createTokenResponse = await post('/auth/createToken', credentials);
      var token = createTokenResponse.data.data.token;
      setToken(token);

      context.commit('authenticationSucceeded', { account: account });
    }
    catch (err) {
      context.commit('authenticationFailed');
      throw err;
    }
  },
  async confirmEmail(context, credentials) {
    try {
      context.commit('authenticationStarted');
      await post('/accounts/confirmEmail', credentials);

      // @NOTE: confirmEmail не возвращает токен, поэтому после подтверждения почты
      //        пользователь должен залогиниться

      context.commit('authenticationCanceled');
    }
    catch (err) {
      context.commit('authenticationFailed');
      throw err;
    }
  },
  async login(context, credentials) {
    try {
      context.commit('authenticationStarted');
      var createTokenResponse = await post('/auth/createToken', credentials);
      var token = createTokenResponse.data.data.token;
      setToken(token);
      var currentAccountResponse = await get('/accounts/current');
      var account = currentAccountResponse.data.data;
      context.commit('authenticationSucceeded', { account: account });
    }
    catch (err) {
      context.commit('authenticationFailed');
      throw err;
    }
  },
  async logout(context) {
    try {
      context.commit('authenticationStarted');
      await post('/auth/invalidateToken');
      clearToken();
      context.commit('authenticationSucceeded', { account: null });
    }
    catch (err) {
      context.commit('authenticationFailed');
      throw err;
    }
  },
  async restorePassword(context, credentials) {
    try {
      context.commit('authenticationStarted');
      await post('/accounts/restorePassword', credentials);
      context.commit('authenticationCanceled');
    }
    catch (err) {
      context.commit('authenticationFailed');
      throw err;
    }
  },
  async changePassword(context, credentials) {
    try {
      context.commit('authenticationStarted');
      await post('/accounts/changePassword', credentials);

      // @NOTE: changePassword не возвращает токен, поэтому после смены пароля
      //        пользователь должен залогиниться

      context.commit('authenticationCanceled');
    }
    catch (err) {
      context.commit('authenticationFailed');
      throw err;
    }
  },
};
