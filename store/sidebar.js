import { get, post, setToken, clearToken } from '@/api';

export function state() {
  return {
    isMe: null,
    user: null,
    userVehicles: null,
    userCommunities: null,
  };
}

export const mutations = {
  userUpdated(state, {user, isMe}) {
    state.user = user;
    state.isMe = isMe;
  },
  userVehiclesUpdated(state, vehicles) {
    state.userVehicles = vehicles;
  },
  userCommunitiesUpdated(state, communities) {
    state.userCommunities = communities;
  },
};

export const actions = {
  async updateAll(context) {
    return Promise.all([
      context.dispatch('loadUser'),
    ]);
  },

  async loadUser(context, id) {
    let currentUserId = _.get(context, 'state.user.id');
    if (currentUserId && currentUserId == id) {
      return;
    }
    if (!id) {
      id = _.get(context.rootState, 'auth.account.id');
    }
    if (!id) {
      console.warn('Пользователь не авторизирован. Информации по текущему пользователю нет.');
      return;
    }

    try {
      let userResponse = await get(`/accounts/${id}/get`);
      context.commit('userUpdated', {
        user: userResponse.data.data,
        isMe: id == _.get(context.rootState, 'auth.account.id'),
      });
      // обновился пользователь - обновляем все связанные с ним данные
      await Promise.all([
        context.dispatch('loadUserVehicles', id),
        context.dispatch('loadUserCommunities', id),
      ]);
    }
    catch (err) {
      console.warn('Не удалось получить данные по пользователю: ', id);
      console.warn('Поведение сайдбара может отличаться от задуманного');
    }
  },

  async loadUserVehicles(context, userId) {
    try {
      let { data: { data: vehicles } } = await get(`/accountVehicles/getAllFull?accountId=${userId}&limit=99`);
      vehicles = _.map(vehicles, vehicle => {
        return _.extend({}, vehicle.accountVehicle, {photo: vehicle.photo});
      });
      context.commit('userVehiclesUpdated', vehicles);
    }
    catch (err) {
      console.warn('Не удалось получить технику пользователя: ', userId);
      console.warn('Поведение сайдбара может отличаться от задуманного');
    }
  },

  async loadUserCommunities(context, userId) {
    let loadedUserId = _.get(context, 'state.user.id');
    try {
      let { data: { data: communities } } = await get(`/communities/joined?accountId=${userId}`);
      context.commit('userCommunitiesUpdated', communities);
    }
    catch (err) {
      console.warn('Не удалось получить сообщества пользователя: ', userId);
      console.warn('Поведение сайдбара может отличаться от задуманного');
    }
  },
};
