export const state = () => ({
  breadcrumbs: [],
});

export const mutations = {
  replaceBreadcrumbs(state, payload) {
    state.breadcrumbs = payload;
  },
};

export const actions = {
  async nuxtServerInit(vuexContext, nuxtContext) {
    if (nuxtContext.isServer) {
      global.NUXT_CONTEXT = nuxtContext;
    }
    await vuexContext.dispatch('analytics/updateCounter');
    await vuexContext.dispatch('auth/update');
    await vuexContext.dispatch('domains/updateAll');
    await vuexContext.dispatch('sidebar/updateAll');
  },
};
