# Notes on MotoTeamRussia Frontend

## Routes

/
    /search

    /news                               article list        
    /:article_category_slug             article list
    /articles                           article list
        /:slug                          article details
    
    /gatherings                         gathering list
        /:slug                          gathering details
    
    /communities                        community list
        /:slug                          community details; community article list
    
    /calendar                           ?
(
    /vehicle-types                      vehicle type list
        /:slug                          vehicle type details; vehicle model list
            /:slug                      vehicle model details; vehicle list
                /:slug                  vehicle details
    
    /brands                             brand list
        /:slug                          brand details; vehicle model list
            /:slug                      vehicle model details; vehicle list
                /:slug                  vehicle details

    /vehicle-models                     vehicle model list
        /:slug                          vehicle model details; vehicle list
            /:slug                      vehicle details

    /vehicles                           vehicle list  with filtering
        /:slug                          vehicle details
)

    /vehicles                           brand list, vehicle type list
        /:brand/:type                   filterable vehicle model list
        /:model                         vehicle model details; vehicle list
            /:vehicle_slug              vehicle

    /profile
        /:slug
            /personal_vehicles
                /:slug
    
    /dashboard
        /personal_vehicles
            /:slug
        /friends
            /:slug
        /private_messages
            /:slug
        /blacklist

    /auth
        /registration
        /login
        /logout
        /password_recovery