var DDF = 'DD.MM.YYYY';
var _ = require('lodash');
var momentRange = require('moment-range');
var moment = momentRange.extendMoment(require('moment'));

var now = moment();
var monthStart = moment().date(1);
var monthEnd = moment().add(1, 'months').date(1).subtract(1, 'days');

console.log(`from: ${monthStart.format(DDF)}, to: ${monthEnd.format(DDF)}`);

var range = moment.range(monthStart, monthEnd);

var days = Array.from(range.by('day', { exclusive: true }));

console.log('days:', days.map(d => d.format(DDF)));

console.log('month: ', now.format('MMMM'));

var eventDate = moment();
_.each(days, d => {
  let range = moment.range(d.clone(), d.clone().add(1, 'day'));
  if (range.contains(eventDate)) {
    console.log('EVENT: ', eventDate.format(DDF+' - hh:mm:ss'), 'at date: ', d.format(DDF+' - hh:mm:ss'));
  }
});
