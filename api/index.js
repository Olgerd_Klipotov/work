import axios from 'axios';
import { API_HOST, API_PORT } from './config';
import { STATIC_HOST, STATIC_PORT } from './config';
import { TOKEN_KEY } from './config';

import Cookies from 'js-cookie';
import Cookie from 'cookie';

const instance = axios.create({
  baseURL: `http://${API_HOST}:${API_PORT}/api/v1/`,
});

// Token stuff

export function getToken() {
  if (global.NUXT_CONTEXT && global.NUXT_CONTEXT.isServer) {
    let cookie = global.NUXT_CONTEXT.hasOwnProperty('COOKIE_OVERRIDE')
      ? global.NUXT_CONTEXT.COOKIE_OVERRIDE
      : global.NUXT_CONTEXT.req.headers.cookie;

    if (!cookie) {
      return;
    }

    try {
      let parsedCookie = Cookie.parse(cookie);
      return parsedCookie[TOKEN_KEY];
    }
    catch (err) {
      console.err(err);
      return;
    }
  }
  else {
    return Cookies.get(TOKEN_KEY);
  }
}

export function setToken(token) {
  if (global.NUXT_CONTEXT && global.NUXT_CONTEXT.isServer) {
    global.NUXT_CONTEXT.res.cookie(TOKEN_KEY, token);
    global.NUXT_CONTEXT.COOKIE_OVERRIDE = token;
  }
  else {
    Cookies.set(TOKEN_KEY, token);
  }
}

export function clearToken() {
  if (global.NUXT_CONTEXT && global.NUXT_CONTEXT.isServer) {
    global.NUXT_CONTEXT.res.clearCookie(TOKEN_KEY);
    global.NUXT_CONTEXT.COOKIE_OVERRIDE = null;
  }
  else {
    Cookies.remove(TOKEN_KEY);
  }
}

instance.interceptors.request.use(function(config) {
  const token = getToken();

  if (token) {
    config.headers['X-AUTH-TOKEN'] = token;
  }

  return config;
});

// Error handling

export function isConnectivityError(err) {
  return err.config && (err.response == undefined);
}

export function isServerError(err) {
  return err.config && err.response && (err.response.status >= 500);
}

export function isApiFailure(err) {
  return err.config && err.response && (err.response.status >= 400) && (err.response.status < 500);
}

// Generic requests

export default instance;

export function getStatic(path, ...rest) {
  return instance.get(`http://${STATIC_HOST}:${STATIC_PORT}/${path}`, ...rest);
}

export function get(...rest) {
  return instance.get(...rest);
}

export function post(...rest) {
  return instance.post(...rest);
}

export function put(...rest) {
  return instance.put(...rest);
}
