export default function(context) {
  if (context.store.state.auth.account === null) {
    return context.redirect('/auth/sign-in');
  }
}
