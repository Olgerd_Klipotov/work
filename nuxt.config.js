var path = require('path');

module.exports = {
  head: {
    htmlAttrs: {
      lang: 'ru',
    },
    // @NOTE: тут бесполезно объявлять title и description, потому что они перетрутся из-за бага с `head` vs `metaInfo`
    meta: [
      {
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
    ],
    link: [
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.0-RC3/css/bootstrap-datepicker.css' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.css' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.css' },
    ],
    script: [
      { src: 'https://cdn.polyfill.io/v2/polyfill.min.js' },
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js' },
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.js' },
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.js' },
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.js' },
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/lang/summernote-ru-RU.js' },
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.js' },
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.0-RC3/js/bootstrap-datepicker.js' },
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.0-RC3/locales/bootstrap-datepicker.ru.min.js' },
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js' },
      { src: 'https://api-maps.yandex.ru/2.1/?lang=ru_RU' },
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/validator/7.0.0/validator.min.js' },
      { src: 'https://www.google.com/recaptcha/api.js' },
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.6.3/tinymce.min.js' },
    ],
  },

  router: {
    middleware: [],
  },

  plugins: [
    '~plugins/auto-register',
    {src: '~plugins/third-party-server', ssr: true},
    {src: '~plugins/third-party-client', ssr: false},
  ],

  loading: true,
  /*
   ** Build configuration
   */
  build: {
    module: {
      rules: [
        {
          test: /\.vue$/,
          use: {
            loader: 'vue-loader',
            options: {
              loaders: {
                // @REFERENCE: https://github.com/vuejs/vue-loader/blob/v11.0.0/docs/en/configurations/pre-processors.md#sass-loader-caveat
                scss: ['vue-style-loader', 'css-loader', 'sass-loader'],
                sass: ['vue-style-loader', 'css-loader', 'sass-loader?indentedSyntax'],
              },
            },
          },
        },
      ],
    },
    extend: function(config) {
      var wp = require('webpack');
      config.resolve.alias['@'] = path.resolve(__dirname);
      config.resolve.alias['vue'] = 'vue/dist/vue.common.js';
      config.plugins.push(new wp.ProvidePlugin({
        Vue: 'vue',
        _: 'lodash',
      }));
    },
    postcss: [
      require('postcss-short')(),
      require('postcss-clearfix')(),
      require('postcss-inline-svg')({
        path: './assets/svg/'
      }),
      require('postcss-flexbugs-fixes')(),
      require('autoprefixer')({
        browsers: ['> 5%']
      })
    ],
  }
};
