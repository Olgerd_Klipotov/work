import Vue from 'vue';
import qs from 'qs';
import blobUtil from 'blob-util';
import { getStatic, get, post } from '@/api';

export function delay(...rest) {
  return new Promise(function(resolve) {
    setTimeout(resolve, ...rest);
  });
}

export function register(req, keyword) {
  var paths = req.keys();

  for (var i = 0; i < paths.length; i++) {
    var path = paths[i];

    var component = req(path).default;
    var name = translatePathToName(path);

    // @NOTE: ignore paths that contain $
    if (name.indexOf('$') !== -1) {
      continue;
    }

    // @NOTE: remove intermediate folders
    var parts = name.split('/');
    name = parts[parts.length - 1];

    // @NOTE: register the component
    Vue.component(name, component);
  }
}

function translatePathToName(filepath) {
  return filepath
    .replace(/^.\//, '') // @NOTE: strip initial `./`
    .replace(/\.vue$/, '') // @NOTE: strip terminal `.vue`
    ;
}

function sampleArray(source, length) {
  if (length === null) return source;

  var target = [];
  while (target.length < length) {
    target = target.concat(source);
  }
  target = target.slice(0, length);
  return target;
}

export async function uploadImage(uri, description = '', targets) {
  let blob = await blobUtil.dataURLToBlob(uri);
  let formData = new FormData();
  formData.append('photo', blob);

  let { data: { data: photo } } = await post(`/photos/upload?altText=${description}`, formData);

  return photo;
}



// используются для перегонки полученного запросом
// в требуемый формат
var MAPPERS = {
  fullArtice: item => _.extend({}, item.article, {photos: item.photos}),
};

export const FeedList = {

  make(rest, pageSize = null, opts = {}) {
    return _.extend({}, {
      rest: rest,
      pageSize: pageSize,
      loaded: false,
      loading: false,
      page: 1,
      pageMax: 1,
      items: null,
      mappers: MAPPERS,
    }, opts);
  },

  async makeAndLoad(rest, pageSize = null, opts = {}) {
    var list = this.make(rest, pageSize, opts);
    await this.load(list);
    return list;
  },

  async resetUrl(list, rest, pageSize = null) {
    list.rest = rest;
    list.pageSize = pageSize;
    list.loaded = false;
    list.page = 1;
    list.pageMax = 1;
    list.items = null;
  },

  async load(list) {
    await this.updatePage(list, 1);
    list.loaded = true;
  },

  async updatePage(list, page, opts) {
    // asyncDAta косячит :(
    list = _.extend(list, {mappers: MAPPERS});

    opts = _.defaults(opts, {
      append: false,
    });

    list.loading = true;

    var urlParts = list.rest.split('?');

    var oldQueryString = urlParts[1];
    var query = qs.parse(oldQueryString);

    if (list.pageSize) {
      query.limit = list.pageSize;
    }
    if (list.items && list.items.length) {
      query.startFromId = _.last(list.items).id;
    }

    var newQueryString = qs.stringify(query);

    var url = urlParts[0] + '?' + newQueryString;

    var articles;
    if (list.rest.startsWith('/fixtures')) {
      let response = await delay(500, getStatic(url));
      articles = sampleArray(response.data, list.pageSize); // @NOTE: симулируем размер странички
    }
    else {
      let { data } = await get(url);
      articles = data.data;
    }

    var mapper = list.mappers[list.mapper];
    if (mapper) {
      articles = _.map(articles, mapper);
    }

    list.loading = false;
    if (opts.append) {
      list.items = (list.items || []).concat(articles);
    }
    else {
      list.items = articles;
    }
    list.page = page;
    if (articles.length == list.pageSize) {
      list.pageMax = Math.max(list.pageMax, list.page + 1);
    }
  },

  async showMore(list) {
    var page = list.page + 1;
    await this.updatePage(list, page, { append: true });
  },

};
