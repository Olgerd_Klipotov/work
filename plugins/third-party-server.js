import MarkdownIt from 'markdown-it';

import '@/assets/main.scss';
import '@/assets/layouts.styl';
import '@/assets/motofont/style.css';

import moment from 'moment';
import 'moment/locale/ru';
// для работы с большим календарем
import { extendMoment } from 'moment-range';
const extendedMoment = extendMoment(moment);

global.DDF = 'DD.MM.YYYY';
global.moment = extendedMoment;

let md = new MarkdownIt();

// lodash
_.mixin({
  // compactMap
  cMap: function(arr, foo) {
      return _.compact(_.map(arr, foo));
  },
  qString: function(obj) {
    var qs = _.reduce(obj, function (result, value, key) {
      if (!_.isNull(value) && !_.isUndefined(value)) {
        if (_.isArray(value)) {
          result += _.reduce(value, function (result1, value1) {
            if (!_.isNull(value1) && !_.isUndefined(value1)) {
              result1 += key + '=' + value1 + '&';
              return result1
            } else {
              return result1;
            }
          }, '')
        } else {
          result += key + '=' + value + '&';
        }
        return result;
      } else {
        return result
      }
    }, '').slice(0, -1);
    return qs;
  },
});

// breadcrumbs
Vue.mixin({
  created() {
    if (this.$options.BREADCRUMBS) {
      Vue.set(this.$root, 'BREADCRUMBS', this.$options.BREADCRUMBS);
    }
  },
});

Vue.mixin({
  data: () => ({
    NO_PHOTO: require('@/assets/img/no-photo.png'),
    EMPTY_PHOTO: require('@/assets/img/empty.png'),
    EMPTY_PHOTO_DARK: require('@/assets/img/empty-dark.png'),
    
    DEFAULT_VEHICLE: {
      typeId: null,
      brandId: null,
      date: new Date(),
      name: 'Без имени',
      review: null,
      features: {
        power: {
          engine: '',
          transmission: '',
          fuelTankCapacity: '',
          displacement: '',
          power: '',
          powerWeightRatio: '',
          torque: '',
          compression: '',
          boreXStroke: '',
          valvesPerCylinder: '',
          fuelSystem: '',
          fuelControl: '',
          ignition: '',
          lubricationSystem: '',
          coolingSystem: '',
          gearbox: '',
          driveline: '',
          fuelConsumption: '',
          starter: '',
          exhaustSystem: '',
          greenhouseGases: '',
        },
        chassis: {
          clutch: '',
          fontSuspension: '',
          rearSuspension: '',
          fontBrakes: '',
          rearBrakes: '',
          tyresOrChains: '',
          wheels: '',
        },
        dimensions: {
          frameType: '',
          wheelbase: '',
          forkAngle: '',
          groundClearance: '',
          seatHeight: '',
          overallHeight: '',
          overallLength: '',
          overallWidth: '',
          fullWeight: '',
          dryWeight: '',
        },
      }
    },

    isMounted: false,
  }),
  mounted() {
    this.isMounted = true;
  },
});

Vue.filter('DDF', function(value) {
  return moment(value).format(DDF);
});

Vue.mixin({
  methods: {
    $time: function(time, format) {
      return moment(time).format(format);
    },
    $humanize: function(time) {
      var duration = moment.duration(moment(time).diff(moment()));
      return duration.humanize(true);
    },
    $truncate: function(text, length = 170, separator = ' ') {
      return _.truncate(text, {
        length: length,
        separator: separator,
      });
    },
    $md: function(...args) {
      return md.render(...args);
    },
    $html: require('to-markdown'),
  },
});

Vue.mixin({
  methods: {

    $domain(domainName) {
      if (_.isString(domainName)) {
        var domain = this.$store.state.domains[domainName];
        if (!domain) {
          console.warn('Домен', domainName, 'не найден');
          return;
        }
        return domain;
      }
      else {
        return domainName;
      }
    },

    $domainValue(domainName, predicate, key) {
      var domain = this.$domain(domainName);
      if (!domain) {
        console.warn('Домен', domainName, 'не найден');
        return;
      }

      var item = _.find(domain, predicate);
      if (!item) {
        console.warn('Значение', predicate, 'не найдено в домене', domainName);
        return;
      }

      return key ? item[key] : item;
    },

    $domainValues(domainName, predicate, mapper) {
      var domain = this.$domain(domainName);
      if (!domain) {
        console.warn('Домен', domainName, 'не найден');
        return;
      }

      var items = _.filter(domain, predicate);

      return mapper ? _.map(items, mapper) : items;
    },

  },
});


Vue.mixin({
  computed: {
    _: __ => _, // lodash

    // ради обратной совместимости.
    ARTICLE_CATEGORIES() {
      return this.$store.state.domains.ArticleCategories;
    },

    authenticated() {
      return this.$store.state.auth.account !== null;
    },
    isAdmin() {
      return this.authenticated;
    },
  },
});
