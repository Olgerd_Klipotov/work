import { register } from '../utils';

var blocks = require.context('../blocks', true, /\.vue$/);
register(blocks, 'block');

var widgets = require.context('../widgets', true, /\.vue$/);
register(widgets, 'widget');
