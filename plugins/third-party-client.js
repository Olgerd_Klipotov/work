import Vue from 'vue';
import store from '@/store';
window.Vue = Vue;

import api from '@/api';
window.api = api;

// https://github.com/kaorun343/vue-youtube-embed
import VueYouTubeEmbed from 'vue-youtube-embed';
Vue.use(VueYouTubeEmbed);

import VueClickaway from 'vue-clickaway';
Vue.mixin(VueClickaway.mixin);
