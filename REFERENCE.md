This document contains WIP documentation for MotoTeamRussia.Ru site frontend.

# Contents

* REST API Reference
* WebSocket API Reference
* Store Architecture


# REST API Reference

First of all, see https://docs.google.com/document/d/1_h2Gl6Pz7VuaTkaydDWibJ4cm37y4Zi8ZSVts1_Zr90 for original API docs.

Well, this API is not actually REST in terms of using HTTP methods properly. Anyways, just keep reading.

## Authentication

Authentication is token-based. Use `/api/auth/createToken` for login and `/api/auth/invalidateToken` for logout.

## Accounts

* registerUser                  `/api/accounts/register`
* confirmCurrentUserEmail       `/api/accounts/confirmEmail`
* getCurrentUser                `/api/accounts/current`
* changeCurrentUserPassword     `/api/accounts/changePass`
* restoreCurrentUserPassword    `/api/accounts/restorePass`
* deleteUser                    `/api/accounts/{id}/delete`
* banUser                       `/api/accounts/{id}/ban`
* subscribeUser                 `/api/accounts/{id}/subscribe`
* unsubscribeUser               `/api/accounts/{id}/unsubscribe`
* getUser                       `/api/accounts/{id}/get`
* updateUser                    `/api/accounts/{id}/update`
* updateUserSettings            `/api/accounts/{id}/updateSettings`
* getUserSubscribers            `/api/accounts/{id}/subscribers`
* getUserFriends                `/api/accounts/{id}/friends`

## Geo

### Countries 

* addCountry                    `/api/countries/add`
* deleteCountry                 `/api/countries/{id}/delete`
* getCountry                    `/api/countries/{id}/get`
* getCountryList                `/api/countries/getAll`

### Cities

* addCity                       `/api/cities/add`
* deleteCity                    `/api/cities/{id}/delete`
* getCity                       `/api/cities/{id}/get`
* findCity                      `/api/cities/find`

## Vehicles

### Brands

* addBrand                      `/api/brands/add`
* getBrandList                  `/api/brands/getAll`
* findBrand                     `/api/brands/find`
* getBrand                      `/api/brands/{id}/get`
* deleteBrand                   `/api/brands/{id}/delete`

### Vehicle Types

* addVehicleType                `/api/vehicleTypes/add`
* getVehicleType                `/api/vehicleTypes/{id}/get`
* deleteVehicleType             `/api/vehicleTypes/{id}/delete`
* getVehicleTypeList            `/api/vehicleTypes/getAll`

### Vehicle Models

* addVehicleModel                `/api/vehicleModels/add`
* getVehicleModel                `/api/vehicleModels/{id}/get`
* deleteVehicleModel             `/api/vehicleModels/{id}/delete`
* findVehicleModel               `/api/vehicleModels/find`

### Vehicle Features

* addVehicleFeature             `/api/features/add`
* getVehicleFeature             `/api/features/{id}/get`
* deleteVehicleFeature          `/api/features/{id}/delete`
* getVehicleFeatureList         `/api/features/vehicles/getAll`
* getVehicleFeatures            `/api/features/vehicles/{id}/getAll` (?)

### Vehicles

* addVehicle                    `/api/vehicles/add`
* getVehicle                    `/api/vehicles/{id}/get`
* deleteVehicle                 `/api/vehicles/{id}/delete`
* getVehicleModelVehicleList    `/api/vehicles/models/{id}/getAll`

### AccountVehicles

* addAccountVehicle             `/api/accountVehicles/add`
* getAccountVehicleList         `/api/accountVehicles/accounts/{id}/getAll`
* getAccountVehicle             `/api/accountVehicles/{id}/get`
* deleteAccountVehicle          `/api/accountVehicles/{id}/delete`
* likeAccountVehicle            `/api/accountVehicles/{id}/like`
* unlikeAccountVehicle          `/api/accountVehicles/{id}/unlike`
* checkAccountVehiclesLiked     `/api/accountVehicles/checkLiked`
* subscribeAccountVehicle       `/api/accountVehicles/{id}/subscribe`
* unsubscribeAccountVehicle     `/api/accountVehicles/{id}/unsubscribe`

## Blogging

### Articles

* addArticle                    `/api/articles/add`
* getArticle                    `/api/articles/{id}/get`
* deleteArticle                 `/api/articles/{id}/delete`
* likeArticle                   `/api/articles/{id}/like`
* unlikeArticle                 `/api/articles/{id}/unlike`
* checkArticlesLiked            `/api/articles/checkLiked`

### Article Categories

* getCategoryArticleList        `/api/articles/categories/getAll`
* subscribeCategory             `/api/articles/categories/subscribe`
* unsubscribeCategory           `/api/articles/categories/unsubscribe`

### User Blogs

* addArticleToUserBlog          `/api/articles/accountBlogs/add` (Current?)
* getUserBlogArticleList        `/api/articles/accountBlogs/{id}/getAll`

### Logbooks

* addArticleToUserLogbook       `/api/articles/logbooks/add` (Current?)
* getUserLogbookArticleList     `/api/articles/logbooks/{id}/getAll`

### Community Blogs

* addArticleToCommunityBlog     `/api/articles/communityBlogs/add` (Which Community?)
* getCommunityBlogArticleList   `/api/articles/communityBlogs/{id}/getAll`

## Comments

* addComment                    `/api/comments/add`
* getObjectCommentList          `/api/comments/{targetType}/{id}/getAll`
* getComment                    `/api/comments/{id}/get`
* deleteComment                 `/api/comments/{id}/delete`
* banComment                    `/api/comments/{id}/ban`
* likeComment                   `/api/comments/{id}/like`
* unlikeComment                 `/api/comments/{id}/unlike`
* checkCommentsLiked            `/api/comments/checkLiked`

## Communities

* addCommunity                  `/api/communities/add`
* getCommunity                  `/api/communities/{id}/get`
* deleteCommunity               `/api/communities/{id}/delete`
* banCommunity                  `/api/communities/{id}/ban`
* joinCommunity                 `/api/communities/{id}/join`
* leaveCommunity                `/api/communities/{id}/leave`

## Gatherings (ex. LiveEvents)

* addGathering                  `/api/liveEvents/add`
* get                           `/api/liveEvents/{id}/get`
* part                          `/api/liveEvents/{id}/get` (typo?)
* delete                        `/api/liveEvents/{id}/delete`

## Claims

* addClaim                      `/api/claims/add`
* getClaim                      `/api/claims/{id}/get`
* rejectClaim                   `/api/claims/{id}/reject`
* completeClaim                 `/api/claims/{id}/complete`
* getClaimList                  `/api/claims/getAll`

## Private Messages

* sendPrivateMessage            `/api/messages/add`
* getChatMessageList            `/api/messages/{receiverId}/getAll`

## Photos

* uploadPhoto                   `/api/photos/upload`
* getPhoto                      `/api/photos/{id}/get`
* deletePhoto                   `/api/photos/{id}/delete`
* resizePhoto                   `/api/photos/{id}/addResize` (?)
* updatePhoto                   `/api/photos/{id}/update`

* likePhoto                     `/api/photos/{id}/like`
* unlikePhoto                   `/api/photos/{id}/unlike`
* checkPhotosLiked              `/api/photos/checkLiked`

create photoalbum? add photo to a photoalbum?
* getPhotoalbumPhotoList        `/api/photos/photoAlbums/{id}/getAll`

* addCalendarPhoto              `/api/photos/calendar/add`
* getCalendarPhotoList          `/api/photos/calendar/{year}/getAll`


# WebSocket API Reference

TODO


# Store Architecture

The store consists of some modules.

## Modules

* auth: auth_token, registration methods, current user
* accounts
* geo
* vehicles
* blogging
* communities
* gatherings
* claims
* private_messages
* photos (?)

## Mutations

Mutations are synchronous changes on store objects, usually simply reflecting API call results.

* `ADD`
* `UPDATE`
* `DELETE`
* `LIKE`
* `UNLIKE`

There are also optimistic mutations happening before the server answers and using `confirmed_by_server: false`.

* `OPTIMISTIC_ADD`
* `OPTIMISTIC_UPDATE`
* `OPTIMISTIC_DELETE`
* `OPTIMISTIC_LIKE`
* `OPTIMISTIC_UNLIKE`

## Actions

Actions are asynchronous operations triggering store updates, usually via API calls.

### Global Action Types

* `FETCH_` — triggers fetching a list of objects from API, like `getCountryList` or `getBrandList`. Often used for data prefetch.
* `ADD_` - performs `OPTIMISTIC_ADD` mutation, then triggers `addSomething` API call and `ADD` mutation after it was resolved. 
* `DELETE_` - performs `OPTIMISTIC_DELETE` mutation, then triggers `deleteSomething` API call and `DELETE` mutation after it was resolved. 
* `LIKE_` - performs `OPTIMISTIC_LIKE` mutation, then triggers `likeSomething` API call and `LIKE` mutation after it was resolved. 
* `UNLIKE_` - performs `OPTIMISTIC_UNLIKE` mutation, then triggers `unlikeSomething` API call and `UNLIKE` mutation after it was resolved. 
* `BAN_` - performs `OPTIMISTIC_BAN` mutation, then triggers `banSomething` API call and `BAN` mutation after it was resolved. 

### Specific Action Types

TODO: more

#### Communities 

* `JOIN_COMMUNITY`
* `LEAVE_COMMUNITY`

## Data Prefetching

Some Data is required globally, while some is needed just for particular pages. In both cases we need to prefetch data during SSR.

### Global Data Prefetching

* `FETCH_COUNTRIES`
* `FETCH_BRANDS`
* `FETCH_VEHICLE_TYPES`
* `FETCH_VEHICLE_FEATURES`
* `FETCH_CURRENT_USER` (if auth_token provided)


# Routes

/
    /auth
        /registration
        /login
        /logout
        /password_recovery
    /dashboard
        /friends
    /im
        /index
        /chat/:account_id
    
    /articles/
        /:article_id

    /profile/:account_username
        /index
        /blog
            /:post_id
        /vehicles
            /:vehicle_id

    /communities

    /gatherings

    /vehicles
        /types/
            /:vehicle_type_id
        /brand/
            /:brand_id
        /models/
            /:model_id
        /filter/
        /:vehicle_id
        /comparison
        
    /communities



# Questions:
1. How to get a list of communities?
2. How to get a list of articles / materials / the latest stuff in blogs?
3. How to get the best review of the week, the most liked photo of the week etc?
4. What about brands, how to separate the first N from 'all brands'?
5. How is "show more" in the account vehicle list supposed to work?
6. In "your friends": how is pagination and "show more" supposed to work?
7. For a particular account, how to get "new in groups" and "new amongs friends"?
8. What about using slugs instead of IDs for accounts and other entities?
9. About the brand list in the sidebar: when inside a vehicle type page, should it work as an inner filter, or should it just lead us to the brand root page?


# Answers / Structure
Sidebar: a list of brands
* probably inside the vehicle type it should work as a double-filter